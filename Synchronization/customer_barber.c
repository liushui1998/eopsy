#include <stdio.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <unistd.h>
#include <stdlib.h>
#define M 4
#define N1 2
#define N2 2
#define N3 2
#define NUM_CUSTOMER 3


union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *arry;
};
struct shared_memory
{
    int customer_female[C1];
    int customer_male[C2];
    int barber_female[N1];
    int barber_man[N2];
    int barber_both[N3];
};
static int semaphore_p(int sem_id, int a)
{
    struct sembuf sem_b;
    sem_b.sem_num = a;
    sem_b.sem_op = -1;//P()
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1)
    {
        fprintf(stderr, "semaphore_p failed\n");
        return 0;
    }
    return 1;
}
static int semaphore_v(int sem_id, int a)
{
    struct sembuf sem_b;
    sem_b.sem_num = a;
    sem_b.sem_op = 1;//V()
    sem_b.sem_flg = SEM_UNDO;
    if(semop(sem_id, &sem_b, 1) == -1)
    {
        fprintf(stderr, "semaphore_v failed\n");
        return 0;
    }
    return 1;
}

int main()
{
//creat shared menory for customer and barbers
    void *shm = NULL;//the address of the shared mamory
    struct shared_memory *shared;//指向shm
    int shmid;//shared memory identifier
    shmid = shmget((key_t)1234, sizeof(struct shared_memory), 0666|IPC_CREAT);
    if(shmid == -1)
    {
        fprintf(stderr, "shmget failed\n");
        exit(EXIT_FAILURE);
    }
    //connect the shared memory to the address space of current proess
    shm = shmat(shmid, 0, 0);
    if(shm == (void*)-1)
    {
        fprintf(stderr, "shmat failed\n");
        exit(EXIT_FAILURE);
    }
    printf("\nMemory attached at %X\n", (int)shm);
    //initialize shared memory
    shm->customer_female;
    shm->customer_male;
    for(int i = 0; i < N1; i++)
        shm->barber_female[i] = -1;
    for(int i = 0; i < N1; i++)
        shm->barber_man[i] = -1;
    for(int i = 0; i < N1; i++)
        shm->barber_both[i] = -1;


    //creat mutex
    int sem_id;
    key_t mutex_key;
    //created semaphore for mutex
    mutex_key = ftok(".",'e');
    int mutex_id =semget((key_t)1024,1,IPC_CREAT | 0666 );
    if(mutex_id < 0)          // check if correctly created
    {
        perror("semget: state mutex not created");
        return 1;
    }
    union semun mutex_union;
    sem_union.val = 1;
    if(semctl(mutex_id, 0, SETVAL, sem_un) < 0)
    {
        perror("semctl: state mutex value failed to set");
        return 1;
    }




    // created semaphore for barbers for checking if waitting customers exist
    int barber_id =semget((key_t)1024,N,IPC_CREAT | 0666 );
    if(barber_id < 0)          // check if correctly created
    {
        perror("semget: state mutex not created");
        return 1;
    }
    union semun barber_union;
    unsigned int zeros[N];
    for(int i = 0; i < N; i++)
    {
        zeros[i] = 0;
    }
    barber_union.array = zeros;
    if(semctl(barber_sems, 0, SETVAL, sem_un) < 0)
    {
        perror("semctl: barber semaphores values failed to set");
        return 1;
    }




    // created semaphore for customers for checking if barbers work or sleep
    int customer_id =semget((key_t)1024,NUM_CUSTOMER,IPC_CREAT | 0666 );
    if(customer_id < 0)          // check if correctly created
    {
        perror("semget: state mutex not created");
        return 1;
    }
    union semun cust_union;
    unsigned int zeros[NUM_CUSTOMER];
    for(int i = 0; i < NUM_CUSTOMER; i++)
    {
        zeros[i] = 0;
    }
    cust_union.array = zeros;
    if(semctl(customer_sems, 0, SETVAL, sem_un) < 0)
    {
        perror("semctl: barber semaphores values failed to set");
        return 1;
    }




    // generat barbers and customers process
    for(int i = 0; i <N+NUM_CUSTOMER; i++)
    {
        pid_t pid = fork();

        if(pid < 0)   //process not created
        {
            perror("Failed to create child process");
            for(term; term<number; term++)
            {
                kill(array[term], SIGTERM);//terminited all the created child process(pid)
            }
            return 1;
        }
        else if(pid > 0)   //parent process
        {
            //store process id
            array[number]=pid;//save the id of created child process
            number++;
        }
        else      //child barber process
        {
            assumption_customer(i,customer_id,mutex_id);
            return 0;
        }
    }
}





//assign the pid to process
void assumption_ barber_customer(int i,int barber_id,int customer_id,int mutex_id)
{
    if(i<N1)
    {
        barber_wm(i,barber_id,mutex_id);
    }
    else if(N1<=i<(N2+N1))
    {
        barber_man(i,barber_id,mutex_id);
    }
    else if((N1+N2)<=i<N)
    {
        barber(i, barber_id,mutex_id);
    }
    else
    {
        customer(i,barber_id,customer_id,mutex_id);
    }
}






void barber_wm(int barber_index,int barber_id,int mutex_id)
{

    semaphore_p(mutex_id,0);
    if(shm->customer_female > 0) // some female customer is waitting
    {
        shm->customer_female--; // the number of waitting for servies reduce
        sleep(2);
        printf("barbers_wm servies for female [pid]: %d", getpid()); // info about cutting
    }
    else// if there is no customers then male barber goes sleep
    {
        for(int a=0; a++; a<=N1)
        {
            if(shm->barber_female[a]==-1)
            {
                barber_index=a;
            }
        }
        printf(" barber_wm index: %d is going to sleep\n", barber_index); // printf info
        semaphore_v(mutex_id,0);   // here have to be unlock before the proccess will go sleep
        semaphore_p(barber_id,barber_index);// barber process go sleep
    }

}





void barber_man(int barber_index,int barber_id,int mutex_id)
{

    semaphore_p(mutex_id,0);
    if(shm->customer_male > 0) // if there are some male clients take him
    {
        shm->customer_female--; // client was taken
        sleep(2);
        printf("barbers_man servies for female [pid]: %d", getpid()); // info about cutting
    }
    else// if there is no customers then male barber goes sleep
    {
        for(int a=0; a++; a<=N2)
        {
            if(shm->barber_man[a]==-1)
            {
                barber_index=a;
            }
        }
        printf(" barber_man index: %d is going to sleep\n", barber_index); // printf info
        semaphore_v(mutex_id,0);   // here have to be unlock before the proccess will go sleep
        semaphore_p(barber_id,barber_index);// barber process go sleep
    }

}

void barber_both(int barber_index, int barber_id,int customer_id, int mutex_id)
{
    semaphore_p(mutex_id,0);
    if(shm->customer_female== 0 && shm->customer_male== 0) // if no customer go sleep
    {
        // the index of sleeping female+male barber is saved in array
        for(int a=0; a++; a<=N3)
        {
            if(shm->barber_both[a]==-1)
            {
                barber_index=a;
            }
        }
        printf("barber_both index: %d :sleep\n", barber_index); // printf info
        semaphore_v(mutex_id,0);     // unlock critical part of code before sleep
        semaphore_p(barber_id,barber_index); // sleep barber process
    }
//if there is customer
    else
    {
        if(shm->customer_female>shm->customer_male)
        {
            shm->customer_female--; // client was taken
            sleep(2);
            printf("barbers_both servies for female [pid]: %d", getpid()); // info about cutting
        }
        else
        {
            shm->customer_male--; // client was taken
            sleep(2);
            printf("barbers_both servies for male [pid]: %d", getpid()); // info about cutting
        }
    }
}



void customer(int customer_index,int barber_id,int customer_id,int mutex_id)
{
    semaphore_p(mutex_id,0);
    pid=getpid();

    if(pid%2==0)//there is female customers
    {


    }
    else //male customer
    {



    }

}
