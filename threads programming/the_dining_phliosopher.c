#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>



#define N 5
#define LEFT (i + N - 1) % N
#define RIGHT (i + 1) % N
#define THINKING 1
#define HUNGRY 2
#define EATING 3

#define up pthread_mutex_unlock
#define down pthread_mutex_lock

int meals=2;
void grab_forks( int i );
void put_away_forks( int i );
void test(int i);
void *philosopher(void *i);
void eat(int i);
void think(int i);

pthread_t philosophers[N];
pthread_mutex_t m;    // main mutex   - initialized to 1
pthread_mutex_t s[N]; // fork mutex   - initialized to 0

int state[N]; // initiated to THINKING's
char *philosophers_names[5] = {"Philosopher0", "Philosopher1", "Philosopher2", "Philosopher3", "Philosopher4"};

//in the test function, we initialization the mutex m to 1 , and mutex s[N] to 0,
// by pthread_create function , create threads
// by pthread_join, we wait the the specialized thread to termined
// by pthread_mutex_destroy, destroy the mutex
int main()
{
    int i;

    int init_result = pthread_mutex_init(&m, NULL);
    int *value = (int*) malloc(sizeof(int)*5);

    for (int i = 0; i < N; i++)
    {
        state[i]=THINKING;                                     //initialization state is THINKING
        init_result = pthread_mutex_init(&s[i], NULL);         // after initialization state of mutex is unlocked
        if(init_result != 0)
        {
            perror("Error: pthread_mutex_init(): Philosopher thread initialization faliure!\n");
            exit(1);
        }
        down(&s[i]);                                           //lock so that forks of philosopher thread will be initialized as not accessible

        value[i] = i;
        int create_result = pthread_create((&philosophers[i]), NULL, philosopher, (value+i));

        if(create_result != 0)
        {
            perror("Error: pthread_create(): Philosopher thread creation faliure!\n");
            exit(1);
        }
    }
    free(value);
    for( i = 0; i < N; ++i)
    {
        int join_result = pthread_join(philosophers[i], NULL); // wait until thread action is finished
        if(join_result != 0)
        {
            perror("Error: pthread_join(): Philosopher thread join faliure!\n");
            exit(1);
        }
        printf("%s finished\n", philosophers_names[i]);
    }
    int destroy_result = pthread_mutex_destroy(&m);             // delete mutex
    if(destroy_result != 0)
    {
        perror("Error: pthread_mutex_destroy(): Main thread destroy faliure!\n");
        exit(1);
    }
    pthread_exit(NULL);

    return 0;
}
// it is the action of philosopher, each philosopher will do the action, think, take the forks, eat, put away the forks
// according to the number of meals, we limited the truns of each philosopher eat meal
void *philosopher(void *i)
{
    int philosopher_id = *(int *) i;
    int a=1;
    while (a<=meals)
    {
        printf("Philosopher[%d] is thinking\n",philosopher_id);
        think(philosopher_id);
        grab_forks(philosopher_id);
        printf("Philosopher[%d] is eating\n",philosopher_id);
        eat(philosopher_id);
        printf("Philosopher[%d] finished his meal[%d]\n",philosopher_id,a);
        a++;
        put_away_forks(philosopher_id);                    //after eating, put down the forks
    }
}
//in the grab_forks function, philosophers visit critical region, and call the test function to get the answer that the philosopher should continue
// if we didn't increase the semaphore at test function in advance, the philosopher will be suspended, and waitting for wake up
void grab_forks(int i)
{
    down(&m);                                             //lock all critical region
    state[i] = HUNGRY;
    test(i);                                              //test if the both neighbor is eating or thinking
                                                        //if the both neighbor is not eating , this philosopher will take the forks
    up(&m);                                               //unlock main thread
    down(&s[i]);                                          // locks the forks so other philosophers can not take them
}
// when the philosopher  after eatting, it's time to put down the forks and wake other suspended thread up and continue his action.
void put_away_forks(int i)
{
    down(&m);
    state[i] = THINKING;
    test(LEFT);                                          //test if the left side philosopher is aviliable to eat food
    test(RIGHT);                                         //test if the right side philosopher is aviliable to eat food
    up(&m);
}
// in the test function, it main work is that check the condition of whether the philosopher will continue his action
void test(int i)
{
                                                         // when then both side neighber are not eating and philosopher is hungry, the two forks are free, and he can eat
    if (state[i] == HUNGRY && state[LEFT] != EATING && state[RIGHT] != EATING)
    {
        state[i] = EATING;
        up(&s[i]);                                      //unclock the forks
    }

}
// sleep for few time to eat
void eat(int i)
{
    sleep(1);
}
// sleep for few time to think
void think(int i)
{
    sleep(2);
}
