#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>


int file_size = 0; //global var to store the file size
//help text
char help_text[] = " copy [-m] <file_name> <new_file_name>   copy [-h] ";

//prototypes of the functions
int copy_mmap(int fd_from, int fd_in);
int copy_read_write(int fd_from, int fd_in);

int main(int argc, char * * argv)
{
    int c = getopt(argc, argv, "m::");//this captures the -options from the parameters, an opt with :: is optional
    if (c == 'h' || argc < 3)  //if the option is 'h' or theres not enough arguments the help text is displayed
    {
        printf("%s\n", help_text);
        return 0;
    }
    else if (c == 'm')    //if we are copying with memory map
    {
        int fd_from = open(argv[2], O_RDONLY);//we create a descriptor with read permits on the original file
        int fd_in = open(argv[3], O_RDWR | O_CREAT, S_IRUSR|S_IWUSR);//then a descriptor that creates/overwrites a file for the new one
        if (fd_from == -1)
        {
            printf("Error Opening the Source File\n");
            return -1;
        }
        else if (fd_in == -1)
        {
            printf("Error Creating/Opening the Output File\n");
            return -1;
        }//we test both descriptors for errors

        copy_mmap(fd_from, fd_in);//call the copy function

        if (close(fd_from) == -1)
        {
            printf("Error Closing Souce File\n");
            return -1;
        }
        else if (close(fd_in) == -1)
        {
            printf("Error Closing Output File\n");
            return -1;
        }//check for errors closing the files
        return 0;
    }
    else
    {
        int fd_from = open(argv[1], O_RDONLY);//we create a descriptor with read permits on the original file
        int fd_in = open(argv[2], O_RDWR | O_CREAT, S_IRUSR|S_IWUSR);
        //check if get the file descripor of source and output file correctly
        if (fd_from == -1)
        {
            printf("Error Opening the Source File\n");
            return -1;
        }
        else if (fd_in == -1)
        {
            printf("Error Creating/Opening the Output File\n");
            return -1;
        }

        struct stat from_stat;//a structure to know the size of the file
        fstat(fd_from, & from_stat);//we load this structure with the info of the source file
        file_size = from_stat.st_size;//take the size value in our global var

        copy_read_write(fd_from, fd_in);//call the copy function
        //close the opend files
        if (close(fd_from) == -1)
        {
            printf("Error Closing Souce File\n");
            return -1;
        }
        else if (close(fd_in) == -1)
        {
            printf("Error Closing Output File\n");
            return -1;
        }
        return 0;
    }
    return -1;//this should be out of bounds
}

int copy_mmap(int fd_from, int fd_in)
{

    struct stat fileStat;
    if (fstat(fd_from, &fileStat) < 0)
    {
        fprintf(stderr, "\nERROR: fstat on the source file failed!\n");
        return 1;
    }
     if (lseek(fd_in,fileStat.st_size, SEEK_SET) ==-1)//set the offseet
    {
        printf("Error moving the cursor on OUTPUT file\n");
        return -1;
    }
    ftruncate(fd_in, fileStat.st_size);//regular file named by fd to be tuncatedto a size of precisely length bytes


    char * from_map = mmap(NULL, fileStat.st_size, PROT_READ, MAP_SHARED, fd_from, 0);//we map the source file for reading
    char * in_map = mmap(NULL, fileStat.st_size, PROT_WRITE, MAP_SHARED, fd_in, 0);//we map the output file for writing
    // check if source file and output file reflected successfully(physical memory reflect to user virtual memory)
    if (from_map == MAP_FAILED)
    {
        printf("Error mapping source file\n");
        return -1;
    }
    else if (from_map == MAP_FAILED)
    {
        printf("Error mapping output file\n");
        return -1;
    }
    if (memcpy(in_map, from_map, fileStat.st_size) == NULL)
    {
        printf("Error copying to output file\n");
        return -1;
    }
    //from memory that souce-map pointed copy file-size bytes to the memory are that in_map pointed
    //clear the relation of related, meanwhile  the access to the original mapped address will result in a segfault
    if (munmap(from_map, fileStat.st_size) == -1)
    {
        printf("Error unmapping source file\n");
        return -1;
    }
    else if (munmap(in_map, fileStat.st_size) == -1)
    {
        printf("Error unmapping ooutput file\n");
        return -1;
    }
    return 0;
}

int copy_read_write(int fd_from, int fd_in)
{
    char * temp = malloc(file_size);//get the specified byte of memory space
    memset(temp,0,file_size);//Initialize memory
    if (temp == NULL)
    {
        printf("theres not enough memory for temp array\n");
        return -1;
    }//check if we had enough memory space
    if (read(fd_from, temp, file_size) < 0)
    {
        printf("Error reading from the source file\n");
        return -1;
    }//we read from the source file in to the buffer
    if (write(fd_in, temp, file_size) != file_size)
    {
        printf("Error partial or wrong writting to the file\n");
        return -1;
    }//then we write from the buffer into the ouput file
    free(temp);//we clear the buffer
    return 0;
}
